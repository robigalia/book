This book contains informal specifications, design notes, and prose explaining
Robigalia.

It is intended to be rendered with AsciiDoctor to HTML. PDF rendering is not
currently reviewed very often, but should be fine.

rosme.adoc is the root file.

CI automatically builds new versions of the book whenever there is a push to
master. You can view them at:

- https://robigalia.gitlab.io/book/rosme.html, from the standard HTML5 backend
- https://robigalia.gitlab.io/book/rosme.xml, from the standard DocBook5
  backend.
- https://robigalia.gitlab.io/book/rosme-fopub.pdf, from fopub via the
  DocBook5 XML.
- https://robigalia.gitlab.io/book/rosme-adoc.pdf, from AsciiDoctor's
  experimental PDF backend.


CC0.
