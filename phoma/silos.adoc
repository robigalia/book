=== Silos

A silo is a sort of container, similar to Solaris zones. Each silo is a mostly
isolated operating system instance in its own right. On initialization, Phoma
consists of a single _root silo_. This silo is responsible for bringing up the
rest of the system based on the configuration it receives from its own creator.
On a Robigalia system, this will be the seL4 root task. Processes can ask the
root silo to create more silos, given some Untyped memory. There are no
technical restrictions on processes creating new silos manually, but creating a
silo is somewhat laborious. Thus it is usually left to the root silo to create
all silos on the system.

Each silo ends up providing a consistent operating environment for the tasks it
hosts. For example, Robigo is a silo type which provides POSIX compatibility.
Building on Robigo, there is a Linux silo which provides compatibility via
either linking to a custom libc or via system call emulation. Phoma also
provides its own silo type. Silo types are not baked into the system, but arise
from convention and simplicity. We'll discuss here native Phoma silos.

Each silo is host to a number of processes. A process consists of a number of
threads, all of which share an address and capability space. Each thread runs an
event loop which processes messages from its inbox. These messages are arbitrary
buffers of bytes, to be interpreted as {capnp} messages. Both the silo itself
and other processes on the system can send messages to a process. If a process
stops processing its event loop, it will possibly be killed and restarted.

Silos are responsible for implementing resource allocation and managing process
capability and address spaces. Processes, on creation, are endowed with some
amount of untyped memory. They can use this untyped memory to create objects,
transfer to other processes, or redeem it for a raw seL4 Untyped object. To get
more Untyped memory, processes must request it from the silo (which may be
denied), or receive it from some other process.
