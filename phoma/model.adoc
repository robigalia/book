=== Operating model

This operating model has evolved informally for a long time, in discussions
between eternaleye and myself (with most good ideas coming from eternaleye).
Originally, it was a first-principles derivation of what a decent storage model
would look like on seL4, given these constraints:

* Structurally avoid denial of service attacks
* Allow revocation of all resources
* Allow delegation of all resources
* Efficiently implementable given seL4's underlying restrictions on untyped
  memory

Of course, I'm enamored with capability systems for providing a simple, robust,
and mathematically sound model of dealing with all of these requirements. The
last requirement was especially tricky, though.

eternaleye pointed me at https://ngnghm.github.io/[Houyhnhnm Computing] quite
early on, which is a absolutely wonderful series of essays about a very
different computing model. I had already examined Urbit at some point and found
it... lacking. Many of the reasons can be found
https://ngnghm.github.io/blog/2016/06/11/chapter-10-houyhnhnms-vs-martians/[here],
written quite elegantly. I am greatly inspired by the vision of a Houyhnhnm
world. Thus, my priorities shifted somewhat to building a good low-level
operating environment for an eventual Houyhnhnm platform. This was my earliest
indication that persistence might live in the core.

Much later, I discovered EROS, Coyotos, and many emails authored by shap
discussing their storage model and designs for L4-HURD. I first started
investigating these systems (and the works of shap in general) by encouragement
from Gernot Heiser and Kevin Elphinstone.

Eventually, I was convinced that orthogonal, transparent persistence belonged in
the system core. This was not an easy conclusion, as it is a huge departure from
both my original goal ("POSIX on seL4") and all operating system design that I
was familiar with. It took a lot of thinking to end up with even the first
version of this architecture. We welcome constructive feedback, especially from
experts on capability systems, especially persistent capability systems.

The whole model is a fairly big, interdependent ball of many ideas. I will do my
best to lay it out here, but eventual educational material will probably need to
be written by someone other than myself, if only for the reason that I am so
deeply aware of the space of trade offs that it becomes difficult to view the
system from the outside.

==== System Overview

At a high level, a Phoma system is composed of two parts: the _firmament_, and
applications. This corresponds loosely to the traditional split of kernelspace
and userland in monolithic operating systems. shap
https://web.archive.org/web/20160412201504/http://www.coyotos.org/docs/misc/eros-structure.pdf[rightly
pointed out (pg 3)] that such a system ought to be considered as three pieces:
the _nucleus_ (traditional kernel), the _kernel_ (nucleus and userland device
drivers), and _applications_. However, given that seL4 calls itself the kernel,
it avoids confusion to everybody to not re-purpose that term.

The firmament is responsible for providing applications the view of a
_persistent_ _capability-based_ world. The underlying system, however, is
usually anything but. All machines today have their running state almost
completely transient, and use a global namespace for many operations. The
"machine interface", from the perspective of the applications, is entirely an
object-capabilities. The firmament instead runs directly on hardware and
communicates with cap-insecure devices. Robigalia provides a firmament that runs
on seL4, and has very strong isolation guarantees. For compatibility, we also
provide a firmament which runs on any tier-1 Rust platform (notably Linux,
Windows, and macOS), with significantly weaker isolation guarantees.

Each application is composed of a number of processes, which in turn are
composed of multiple threads. Each thread hosts a number of objects. A thread
has a virtual address space, a badge space, and a capability space. The virtual
address space is the mapping from its addresses to physical memory. The badge
space is its internal mapping of the objects external capabilities refer to. The
capability space is used for storing capabilities to external objects. These
correspond approximately to near and far references in E.

TODO: transition

==== Storage

The storage subsystem is the most important, and complex, part of Robigalia. It
provides all system state. The model is largely simple (TODO: I think/hope), but
the implementation can be fiendishly complex. This model is largely inspired by
EROS and Coyotos (although it was arrived at largely independently).

===== Permanent Storage

Permanent (information) storage is provided by Nemo, which uses nameless writes as
the communication primitive for reading and storing information. Nameless writes
are a (somewhat) new technique for managing storage, introduced to deal with the
limitations of flash storage devices. Fundamentally, they are quite simple:

TODO: nameless interface

On top of this, a logical volume manager and so forth are implemented. Nemo
internally has an implementation of nameless writes onto devices which don't
natively support such an interface (such as most HDDs). Devices do not need to
be drives attached to the local system, any object store, such as Tahoe-LAFS or
Lustre?

There is a well-defined on-disk format for the pickled state of objects in the
system.

TODO: Should probably inspect SCSI OSD to see what it has to offer in this
space. It has a crypto capability system, and provides object storage. The
technology doesn't seem very widely available as HW. See:

* http://www.snia.org/sites/default/education/tutorials/2013/spring/file/BrentWelch_Object_Storage_Technology.pdf

===== Transient Storage

Transient storage, on the other hand, is managed entirely by the firmament, and
is firmament-specific. It only needs to expose the required interfaces to the
rest of the system, and uphold the system's invariants. Further, it's the job of
the firmament for persist all system state according to its configuration (say,
every 5 minutes).

This mostly includes memory and page tables. The

==== Space Banks

Space banks (name taken from EROS, although our inspiration was seL4's Untyped
objects) are how the system does resource accounting. They are called banks to
invoke a visceral understanding of how resource accounting works on a robust
system: you can only spend what you have, and you can only get more from someone
else. We will often use analogies to (a gross simplification of) real-world
monetary systems to convey intuitions about resources.

Space banks allow these operations:

[source,rust]
----
trait SpaceBank {
  fn buy_page_region(&self, bytes: u64) -> Result<Region>;
  fn sell_page_region(&self, region: Region);
  fn remaining_quota(&self) -> u64;
  fn effective_quota(&self) -> u64;
  fn destroy(&self);
  fn child_bank(&self, quota: u64) -> Result<SpaceBank>;
  fn verify(&self, other: Cap) -> bool;
}
----

You can _buy_ a region from the bank, which gives you more storage space for
information. You can also _sell_ your regions back to the bank. You pay (and the
bank compensates) in _quota_. Every space bank has associated with it a fixed
quota, and tracks how much you have bought using that quota. You can create
sub-banks using `child_bank`. The child bank can have an arbitrary quota.
However, in practice, it will only be able to use the minimum of all the
remaining quota in its transitive parents, up to the Prime Bank.

Destroying a bank will revoke all access to objects allocated from it. When an
object is destroyed, the resources used to pay for it are returned to the quota.

You can also ask a space bank to determine if it recognizes some other
capability as being a space bank with some common heritage. This is useful in
the core of the system, but not much elsewhere.

Why can space banks have a quota larger than their parent? Simple! For
implementing non-robust systems. Really. This is known as "overcommit" in
traditional operating systems. The OS pretends it has more resources than it
actually does, so that allocation requests don't initially fail. A process can
"reserve" address space, but when it actually goes to use it, the OS may be out
of memory and swap and it will fault and die if your siblings happened to be
particularly greedy at that point in time. Robustness destroyed. But, while
robustness is a useful property of vital system components, it's often not that
important to some user applications.

To use the space bank to create other, non-information storage objects, you'll
need to use an appropriate Creator, and pass it a space bank.

NOTE: This is most similar to Coyotos, but see the EROS documentation:

* https://gnunet.org/sites/default/files/storedesign2002.pdf
* https://lists.gnu.org/archive/html/l4-hurd/2005-10/msg00043.html
* https://web.archive.org/web/20150915053026/http://www.eros-os.org/essays/Persistence.html

==== IPC

==== Processes, Badges, Objects, and IPC

Processes in Phoma are a group of threads which share a capability, address, and
badge space (or "bspace"). The bspace describes how a process allocates and uses
badges for identifying mutually-isolated "bushels" of objects.

Most important principle: client pays for storage.

When a process is created, its initialized environment usually contains at least
two capabilities: its _object endpoint_ (OEP) and _parent endpoint_ (PEP). The
OEP is the raw, unbadged endpoint capability that the server will receive ~all
of its messages on. It should hold this capability very tightly, and only ever
give out freshly minted badged copies, as anyone with the raw OEP can forge
connections. The _parent endpoint_ is an endpoint that the process can use to
communicate with its creator. It can request further authority ("powerbox
request"), or offer up a capability it would like shared with some party (for
example, a capability to create objects). It's vital to the security of the
whole setup that new processes are always completely confined, and only endowed
with authority granted by the parent.

TODO: clean this up, presentation wise.

A bushel is represented by some state in the process which provides it, and is
accessed by other processes via a badged endpoint. This endpoint is the input to
a state machine which decides how to process the message.

Creating bushels is done by sending messages to a special _creator_ capability,
which some servers can offer. This is largely application-defined. The first
message sent to a creator transfers a capability to a space bank, as well as any
other information needed to allocate the bushel. The server will reply with
"success" or "fail", and other information needed to continue the protocol. In
the case of success, it will also transfer a badged capability to the new
bushel.

Internally, the server is parameterized by an `n`, which is a number between
`PAGE_BITS` and `WORD_BITS - BADGE_BITS`. `PAGE_BITS` is typically 12,
`WORD_BITS` is either 32 or 64, and `BADGE_SIZE` is 28. `n - PAGE_BITS` is the
size (in 4K pages) of a fixed-length _slab_, which stores the initial
bookkeeping and state for a bushel. Later dynamic allocations from the space
bank are allowed, and pointers outside of the slab are also allowed.

The IPC buffer for a bushel can also be extended. If a message larger than the
standard seL4 IPC buffer is required, the client can negotiate _extended virtual
message registers_ (EVMRs). At that point, all messages should be written into
the EVMR before sending, with only physical message registers used for the
actual endpoint send operation.
